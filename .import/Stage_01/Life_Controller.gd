extends Node

var player
export(Image) var gray_heart
export(Image) var heart
var parent

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	parent = get_parent()
	player = parent.get_node("Player")

func _process(_delta):
	if player.life == 3:
		get_child(0).texture = heart
		get_child(1).texture = heart
		get_child(2).texture = heart
	if player.life == 2:
		get_child(0).texture = heart
		get_child(1).texture = heart
		get_child(2).texture = gray_heart
	if player.life == 1:
		get_child(0).texture = heart
		get_child(1).texture = gray_heart
		get_child(2).texture = gray_heart
	if player.life == 0:
		get_child(0).texture = gray_heart
		get_child(1).texture = gray_heart
		get_child(2).texture = gray_heart
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
