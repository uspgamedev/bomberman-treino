extends Node2D

var right_raycast
var bottom_raycast
var left_raycast
var up_raycast

var instantiated = false

var raycasts = [right_raycast,
			bottom_raycast,
			left_raycast,
			up_raycast]
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	instantiated = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if instantiated:
		for i in range(raycasts.size()):
			print("it's", i)
			if get_child(i).is_colliding():
				print("Entered is colliding")
				if get_child(i).get_collider().is_in_group("tilemap"):
					match i:
						0:
							get_parent().get_node("fire_right").queue_free()
						1:
							get_parent().get_node("fire_bottom").queue_free()
						2:
							get_parent().get_node("fire_left").queue_free()
						3:
							get_parent().get_node("fire_up").queue_free()
					instantiated = false;
