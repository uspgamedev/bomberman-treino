extends Node

var player
export(Image) var gray_heart
export(Image) var heart
export(PackedScene) var play_again_popup
var popup_position = Vector2(152, 104)
var parent
var life_is_not_zero = false

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	parent = get_parent()
	player = parent.get_node("Player")

func _process(_delta):
	if !life_is_not_zero:
		if player.life == 3:
			get_child(0).texture = heart
			get_child(1).texture = heart
			get_child(2).texture = heart
		if player.life == 2:
			get_child(0).texture = heart
			get_child(1).texture = heart
			get_child(2).texture = gray_heart
		if player.life == 1:
			get_child(0).texture = heart
			get_child(1).texture = gray_heart
			get_child(2).texture = gray_heart
		if player.life == 0:
			get_child(0).texture = gray_heart
			get_child(1).texture = gray_heart
			get_child(2).texture = gray_heart
			var popup = play_again_popup.instance()
			popup.position = popup_position
			popup.winner_player = 2
			get_parent().add_child(popup)
			life_is_not_zero = true
		
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
