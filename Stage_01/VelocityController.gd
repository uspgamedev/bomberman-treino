extends Node2D
#Este script é responsável por multiplicar a velocidade dos jogadores conforme o jogo passa
export(bool) var affect_explosion_timing
export(float) var velocity_threshold
export(float) var velocity_incresing_per_second
var player1
var player2
var musica
var timer = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	player1 = get_parent().get_node("Player")
	player2 = get_parent().get_node("Player2")
	if affect_explosion_timing:
		player1.affect_explosion_timing = true 
		player2.affect_explosion_timing  = true

func _process(delta):
	musica = get_parent().get_node("AudioStreamPlayer")
	timer = timer + 0.001
	print(timer)
	if player1.speed < velocity_threshold:
		player1.speed = player1.speed + (velocity_incresing_per_second * delta)
		player2.speed = player2.speed + (velocity_incresing_per_second * delta)
		if timer >= 1:
			timer = 0
			musica.set_pitch_scale(musica.get_pitch_scale() + velocity_incresing_per_second*delta)
		
