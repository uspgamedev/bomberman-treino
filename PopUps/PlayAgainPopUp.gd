extends Sprite

var winner_player
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	get_child(0).set_text("Player {} Won!".format([winner_player], "{}"))


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _on_PlayAgainButton_pressed():
	get_tree().reload_current_scene()
