extends Node

var right_raycast
var bottom_raycast
var left_raycast
var up_raycast
var count = 0
var instantiated = false
var freed_instances = false

var raycasts = [right_raycast,
			bottom_raycast,
			left_raycast,
			up_raycast]
			
var explosion_ray_animations = []
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	instantiated = true
	explosion_ray_animations = [get_parent().get_node("explosion_ray_anim2_right"),
								get_parent().get_node("explosion_ray_anim2_top"),
								get_parent().get_node("explosion_ray_anim2_bottom"),
								get_parent().get_node("explosion_ray_anim2_left"),
								get_parent().get_node("explosion_ray_anim_right"),
								get_parent().get_node("explosion_ray_anim_top"),
								get_parent().get_node("explosion_ray_anim_bottom"),
								get_parent().get_node("explosion_ray_anim_left")]


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if instantiated:
		for i in range(raycasts.size()):
			if get_child(i).is_colliding():
				if get_child(i).get_collider().is_in_group("tilemap"):
					match i:
						0:
							get_parent().get_node("explosion_ray_anim2_right").visible = false
							get_parent().get_node("explosion_ray_anim_right").visible = false
							count = count + 1
						1:
							get_parent().get_node("explosion_ray_anim2_bottom").visible = false
							get_parent().get_node("explosion_ray_anim_bottom").visible = false
							count = count + 1
						2:
							get_parent().get_node("explosion_ray_anim2_left").visible = false
							get_parent().get_node("explosion_ray_anim_left").visible = false
							count = count + 1
						3:
							get_parent().get_node("explosion_ray_anim2_top").visible = false
							get_parent().get_node("explosion_ray_anim_top").visible = false
							count = count + 1
					instantiated = false
	for explosion_ray in explosion_ray_animations:
		explosion_ray.playing = true
