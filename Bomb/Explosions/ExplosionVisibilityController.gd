extends Node

var right_raycast
var bottom_raycast
var left_raycast
var up_raycast


#esta variável representa quantas vezes o raycast atingiu algo
var count = 0


var instantiated = false

var raycasts = [right_raycast,
			bottom_raycast,
			left_raycast,
			up_raycast]

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	instantiated = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if instantiated:
		for i in range(raycasts.size()):
			if get_child(i).is_colliding():
				if get_child(i).get_collider().is_in_group("tilemap"):
					match i:
						0:
							get_parent().get_node("explosion_ray_anim2_right").visible = false
							get_parent().get_node("explosion_ray_anim_right").visible = true
							count += 1
						1:
							get_parent().get_node("explosion_ray_anim2_bottom").visible = false
							get_parent().get_node("explosion_ray_anim_bottom").visible = true
							count += 1
						2:
							get_parent().get_node("explosion_ray_anim2_left").visible = false
							get_parent().get_node("explosion_ray_anim_left").visible = true
							count += 1
						3:
							get_parent().get_node("explosion_ray_anim2_top").visible = false
							get_parent().get_node("explosion_ray_anim_top").visible = true
							count += 1
					instantiated = false
