extends Node2D

var bomb_owner
var explosion_ray_animations = []
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	explosion_ray_animations = [get_node("explosion_ray_anim_right"),
								get_node("explosion_ray_anim_top"),
								get_node("explosion_ray_anim_bottom"),
								get_node("explosion_ray_anim_left")]
	for explosion_ray in explosion_ray_animations:
		explosion_ray.playing = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Timer_timeout():
	bomb_owner.exploding = false
	bomb_owner.queue_free()
	queue_free()
	
