extends Area2D

func _ready():
	pass 

func _on_PowerUp1_body_entered(body):
	if body.is_in_group("player"):
		body.bombs_range = 2
		queue_free()
