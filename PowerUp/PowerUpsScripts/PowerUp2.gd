extends Area2D

func _ready():
	pass 

func _on_PowerUp1_body_entered(body):
	if body.is_in_group("player"):
		if body.life < 3:
			body.life = body.life + 1
		queue_free()
